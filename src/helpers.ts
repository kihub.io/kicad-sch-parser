import { LibraryItem } from "./kicad-schematic-types";

// Based on LIB_ID::FixIllegalChars
export function replaceIllegalChars(s: string): string {
    const isLegalChar = c => (c.charCodeAt(0) >= 32) &&  (c != ' ') && (c != ':') && (c != '/');
    return s
        .split('')
        .map(c => isLegalChar(c)? c : '_')
        .join('');
}

// Based on LIB_ID::Parse
export function parseLibraryItem(s: string, formatVersion: number): LibraryItem {

    let name = s.split('~').join(' ');
    let libNickname: null | string = null;
    let revision: null | string = null;

    if (formatVersion > 3) {
      // Parse the lib name, per lib_id.cpp, LIB_ID::Parse funcion

      const revSepIndex = name.indexOf('/');
      if (revSepIndex > 0) {
        // Revision specified
        revision = name.slice(revSepIndex + 1);
        name = name.slice(0, revSepIndex);
      }
      else {
        // Revision not specified
        revision = null;
      }

      const nickSepIndex = name.indexOf(':');
      if (nickSepIndex > 0) {
        // Library nickname specified
        libNickname = name.slice(0, nickSepIndex);
        name = replaceIllegalChars(name.slice(nickSepIndex + 1));
      }
      else {
        // Library nickname not specified
        name = replaceIllegalChars(name);
        libNickname = null;
      }
    }
    else {
      revision = null;
      libNickname = null;
    }

    return { name, revision, libNickname };
}

// Based on SCH_LEGACY_PLUGIN::loadComponent
export function parseRefDes(raw: string): string {
    const r = raw
        .split('')
        .map(c => c === '~'? ' ': c)
        .reverse()

    // Leave only the non-numeric part of the refdes
    const i = r.findIndex(c => (c < '0' || c > '9') && (c != '?'));
    return (i > 0? r.slice(i) : r).reverse().join('');

}

// By default, all measures are in mil. This function converts mm to mil
// (include/base_units.h, Mm2mils function)
const mm = x => Math.round(x * 1000.0/25.4);

export interface PageInfo {
    pageSize: string;
    width: number;
    height: number;
    portrait?: boolean;
}

// Source: common/page_info.cpp
const pageTable: { [key: string ]: { width: number, height: number } } = {
    "A4":       { height: mm(297),   width: mm(210) },
    "A3":       { height: mm(420),   width: mm(297) },
    "A2":       { height: mm(594),   width: mm(420) },
    "A1":       { height: mm(841),   width: mm(594) },
    "A0":       { height: mm(1189),  width: mm(841) },
    "A":        { height: 11000,     width: 8500    },
    "B":        { height: 17000,     width: 11000   },
    "C":        { height: 22000,     width: 17000   },
    "D":        { height: 34000,     width: 22000   },
    "E":        { height: 44000,     width: 34000   },
    "GERBER":   { height: 32000,     width: 32000   },
    "USLetter": { height: 11000,     width: 8500    },
    "USLegal":  { height: 14000,     width: 8500    },
    "USLedger": { height: 17000,     width: 11000   }
}


/**
 * Returns page size information.
 * 
 * @param sizeCode One of the predefined page sizes
 * @param width The width of the page, in mils (will only be user with custom page size)
 * @param height The height of the page, in mils (will only be user with custom page size)
 */
export function calcPageSize(sizeCode: string, width: number, height: number, portrait: boolean): PageInfo {

    if (sizeCode === 'User')
        return {
            pageSize: 'User',
            width, height
        }
    
    else {
        const e = pageTable[sizeCode];
        if (!e)
            throw new Error('Invalid page size');

        return {
            pageSize: sizeCode,
            height: e.height,
            width: e.width,
            portrait
        };
    }

}

/**
 * Replaces the "\n" sequence in a string with newline.
 * 
 * Reference: bottom of SCH_LEGACY_PLUGIN::loadText method in sch_legacy_plugin.cpp
 * 
 * @param s The source string.
 * @returns The processed string.
 */
export function replaceNewline(s: string): string {
  return s.split('\\n').join('\n');
}