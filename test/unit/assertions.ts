
import LikeHelper from 'chai-like';
import { parse, SyntaxError } from '../../src/generated/parser';
import { Schematic } from '../../src/kicad-schematic-types';

export default function ( chai, utils ) {

    chai.use( LikeHelper );

    const Assertion = chai.Assertion;

    Assertion.addMethod( "parseAs", function ( schExpected: Schematic ) {

        const schText = utils.flag(this, "object");
        const kiSch = parse( schText );

        new Assertion( kiSch ).like( schExpected );

    } );

    Assertion.addMethod( "failParsingWith", function ( expectedMessage: string ) {

        const schText = utils.flag(this, "object");
        let pass = false;

        try {
            parse( schText );
        }
        catch(e) {
            if ((e instanceof Error) && (e.message === expectedMessage)){
                pass = true;
            }
            else
                this.assert(false,
                    `expected #{this} to throw SyntaxError(${expectedMessage})`,
                    null,
                    null,
                    e.message + 'b'                   
                )
        }

        this.assert(
            pass,
            "expected #{this} to throw but it didn't",
            "expected #{this} to not report an error",
            null,
            null
        );

    } );

    Assertion.addMethod( "parseWithoutError", function () {

        const schText = utils.flag(this, "object");

        try {

            parse( schText );

        }
        catch(e) {
            const line = (<SyntaxError>e).location.start.line;
            this.assert(
                false,
                `expected to parse without error, but found error on line ${line} column ${e.location.start.column}`,
                "expected to fail parsing with error",
                null,
                null
            );

        }

    } );
}

