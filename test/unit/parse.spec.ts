/// <reference path="./assertions.d.ts"/>

import * as chai from 'chai';
import { readFile } from 'fs-extra';
import { resolve } from 'path';
import kischHelper from './assertions';
import { ItemType, FieldOrientation, HorizontalAlignment, VerticalAlignment, ComponentItem, TextOrientation, 
    TextItemType, SheetLabelType, LineType, LineStyle, Position, SheetPinSide } from '../../src/kicad-schematic-types';
import { parse } from '../../src/generated/parser';

chai.use(kischHelper);
const { expect } = chai;

const fileHeader = [
    'EESchema Schematic File Version 4',
    'EELAYER 10 20',
    'EELAYER END',
    '$Descr A4 100 100',
    '$EndDescr',
];

describe('Schematic Parser', function() {

    it('Minimal schematic', function() {

        const sch = [
            'EESchema Schematic File Version 2',
            'EELAYER 10 20',
            'EELAYER END',
            '$Descr A4 100 100',
            '$EndDescr',
            '$EndSCHEMATC'
        ].join('\n');

        expect(sch).to.parseAs({
            formatVersion: 2,
            libs: [],
            descr: {},
            items: [],       
        });
    });

    describe('Description Block', function() {

        it('Empty - Std page size', function() {
            const sch1 = [
                'EESchema Schematic File Version 2',
                'EELAYER 10 20',
                'EELAYER END',
                '$Descr A4 100 200',
                '$EndDescr',
                '$EndSCHEMATC'
            ].join('\n');
    
            expect(sch1).to.parseAs({
                descr: {
                    pageInfo: {
                        pageSize: 'A4',
                        width: 8268,
                        height: 11693,
                        portrait: false
                    }
                },           
            });

            const sch2 = [
                'EESchema Schematic File Version 2',
                'EELAYER 10 20',
                'EELAYER END',
                '$Descr E 100 200 portrait',
                '$EndDescr',
                '$EndSCHEMATC'
            ].join('\n');
    
            expect(sch2).to.parseAs({
                descr: {
                    pageInfo: {
                        pageSize: 'E',
                        width: 34000,
                        height: 44000,
                        portrait: true
                    }
                },           
            });

        });

        it('Empty - User page size', function() {
            const sch = [
                'EESchema Schematic File Version 2',
                'EELAYER 10 20',
                'EELAYER END',
                '$Descr User 1000 2000',
                'Title "Descr Title"',
                'Sheet 10 20',
                '$EndDescr',
                '$EndSCHEMATC'
            ].join('\n');
    
            expect(sch).to.parseAs({
                descr: {
                    pageInfo: {
                        pageSize: 'User',
                        width: 1000,
                        height: 2000
                    }
                },           
            });
            
        });

        it('Partial', function() {
            const sch = [
                'EESchema Schematic File Version 2',
                'EELAYER 10 20',
                'EELAYER END',
                '$Descr A4 100 200',
                'Title "Descr Title"',
                'Sheet 10 20',
                'Comment2 "Some comment"',
                '$EndDescr',
                '$EndSCHEMATC'
            ].join('\n');
    
            expect(sch).to.parseAs({
                descr: {
                    title: 'Descr Title',
                    sheetNumber: 10,
                    totalSheets: 20,
                    comment2: 'Some comment'
                },           
            });           
        });

        it('Full', function() {
            const sch = [
                'EESchema Schematic File Version 2',
                'EELAYER 10 20',
                'EELAYER END',
                '$Descr User 1000 2000',
                'encoding utf-8',
                'Sheet 1 4',
                'Title "Some Title"',
                'Date "01-02-03"',
                'Rev "A"',
                'Comp "Company Ltd."',
                'Comment1 "Some comment 1"',
                'Comment2 "Some comment 2"',
                'Comment3 "Some comment 3"',
                'Comment4 "Some comment 4"',
                '$EndDescr',
                '$EndSCHEMATC'
            ].join('\n');
    
            expect(sch).to.parseAs({
                descr: {
                    pageInfo: {
                        pageSize: 'User',
                        width: 1000,
                        height: 2000
                    },
                    title: 'Some Title',
                    sheetNumber: 1,
                    totalSheets: 4,
                    date: '01-02-03',
                    rev: 'A',
                    company: 'Company Ltd.',
                    comment1: 'Some comment 1',
                    comment2: 'Some comment 2',
                    comment3: 'Some comment 3',
                    comment4: 'Some comment 4',
                },           
            });
            
        });



    });

    describe('Component Item', function() {

        const sch = (extra = [], extraFields = []) => ([
            ...fileHeader,
            '$Comp',
            'L 74xx_IEEE:7400 U?',
            'U 4 1 5CC6394D',
            'P 1111 2222',
            ...extra,
            'F 0 "U3" H 13850 8350 60  0000 L BNN',
            'F 1 "K4B4G1646Q-HYK0(FBGA-96_256Mx16_DDR3-1600_11-11-11)" V 12800 2200 50  0000 R TIB',
            'F 2 "OLIMEX_IC-FP:FBGA96(HYNIX_SAMSUNG_512MX16_DDR3)" H 14168 5126 20  0001 C CNN',
            'F 3 "" H 14100 5200 60  0000 C CNN',
            ...extraFields,
            '\t1 0 0',
            '\t0 0 0 0',
            '$EndComp',
            '$EndSCHEMATC'
        ]).join('\n');


        it('L line', function() {
    
            expect(sch()).to.parseAs({
                formatVersion: 4,
                items: [
                    {
                        type: ItemType.Component,
                        libraryItem: {
                            name: '7400',
                            libNickname: '74xx_IEEE',
                        },
                        refDes: 'U'
                    }
                ],
            });
    
        });

        it('U line', function() {
    
            expect(sch()).to.parseAs({
                formatVersion: 4,
                items: [
                    {
                        type: ItemType.Component,
                        unit: 4,
                        convert: 1,
                        timestamp: '5CC6394D'
                    }
                ],
            });
    
        });

        it('AR line', function() {
            expect(sch([
                'AR Path="abc" Ref="def" Part="4"',
                'AR Path="ghi" Ref="jkl" Part="5"'
            ])).to.parseAs({
                formatVersion: 4,
                items: [
                    {
                        type: ItemType.Component,
                        hierReference: [
                            {
                                rpath: 'abc',
                                ref: 'def',
                                part: 4
                            },
                            {
                                rpath: 'ghi',
                                ref: 'jkl',
                                part: 5
                            }
                        ]
                    }
                ]
            })
        });

        it('P line', function() {

            expect(sch()).to.parseAs({
                items: [
                    {
                        type: ItemType.Component,
                        position: { x: 1111, y: 2222 }
                    }
                ]
            })

        });

        describe('Fields', function() {
           
           it('Mandatory fields', function() {
               expect(sch()).to.parseAs({
                   items: [
                       {
                           fields: [
                               //  F 0 "U3" H 13850 8350 60  0000 L BNN
                               {
                                   index: 0,
                                   value: 'U3',
                                   position: { x: 13850, y: 8350 },
                                   size: 60,
                                   orientation: FieldOrientation.Horizontal,
                                   visible: false,
                                   halign: HorizontalAlignment.Left,
                                   valign: VerticalAlignment.Bottom,
                                   italic: false,
                                   bold: false
                               },
                               // F 1 "K4B4G1646Q-HYK0(FBGA-96_256Mx16_DDR3-1600_11-11-11)" V 12800 2200 50  0000 R TIB
                               {
                                    index: 1,
                                    value: 'K4B4G1646Q-HYK0(FBGA-96_256Mx16_DDR3-1600_11-11-11)',
                                    position: { x: 12800, y: 2200 },
                                    size: 50,
                                    orientation: FieldOrientation.Vertical,
                                    visible: false,
                                    halign: HorizontalAlignment.Right,
                                    valign: VerticalAlignment.Top,
                                    italic: true,
                                    bold: true
                               },
                               // F 2 "OLIMEX_IC-FP:FBGA96(HYNIX_SAMSUNG_512MX16_DDR3)" H 14168 5126 20  0001 C CNN
                               {
                                    index: 2,
                                    value: 'OLIMEX_IC-FP:FBGA96(HYNIX_SAMSUNG_512MX16_DDR3)',
                                    position: { x: 14168, y: 5126 },
                                    size: 20,
                                    orientation: FieldOrientation.Horizontal,
                                    visible: true,
                                    halign: HorizontalAlignment.Center,
                                    valign: VerticalAlignment.Center,
                                    italic: false,
                                    bold: false
                               },
                               // F 3 "" H 14100 5200 60  0000 C CNN
                               {
                                    index: 3,
                                    value: '',
                                    position: { x: 14100, y: 5200 },
                                    size: 60,
                                    orientation: FieldOrientation.Horizontal,
                                    visible: false,
                                    halign: HorizontalAlignment.Center,
                                    valign: VerticalAlignment.Center,
                                    italic: false,
                                    bold: false
                               }
                           ]
                       }
                   ]
               });

           });

           it("User fields", function() {
                expect(sch([], [
                    'F 4 "Value1" H 100 100 10 0000 C CNN "UserField1"',
                    'F 5 "Value2" H 100 100 10 0000 C CNN "UserField2"',
                ])).to.parseAs({
                    items: [
                        {
                            fields: [
                                { index: 0 },
                                { index: 1 },
                                { index: 2 },
                                { index: 3 },
                                { 
                                    index: 4,
                                    name: 'UserField1',
                                    value: 'Value1'
                                },
                                { 
                                    index: 5,
                                    name: 'UserField2',
                                    value: 'Value2'
                                },
                            ]
                        }
                    ]
                });
           });

           it('Default Text Attrs', function() {
                expect([
                    ...fileHeader,
                    '$Comp',
                    'L 74xx_IEEE:7400 U?',
                    'U 4 1 5CC6394D',
                    'P 1111 2222',
                    'F 0 "" H 100 100 10  0000',
                    'F 1 "" V 100 100 10  0000 R T',
                    'F 2 "" H 100 100 10  0001 C CNN',
                    'F 3 "" H 100 100 10  0000 C CNN',
                    '\t1 0 0',
                    '\t0 0 0 0',
                    '$EndComp',
                    '$EndSCHEMATC'
                ].join('\n')).to.parseAs({
                    items: [
                        {
                            fields: [
                                {
                                    index: 0,
                                    halign: HorizontalAlignment.Center,
                                    valign: VerticalAlignment.Center,
                                    italic: false,
                                    bold: false
                                },
                                {
                                    index: 1,
                                    halign: HorizontalAlignment.Right,
                                    valign: VerticalAlignment.Top,
                                    italic: false,
                                    bold: false
                                },
                                {
                                    index: 2
                                },
                                {
                                    index: 3
                                }
                            ]   
                        }
                    ]
                });
           });


           it('Ignore field name on mandatory fields', function() {
                const sch = [
                    ...fileHeader,
                    '$Comp',
                    'L 74xx_IEEE:7400 U?',
                    'U 4 1 5CC6394D',
                    'P 1111 2222',
                    'F 0 "" H 100 100 10  0000 C CNN "FieldName"',
                    'F 1 "" V 100 100 10  0000 C CNN',
                    'F 2 "" H 100 100 10  0001 C CNN',
                    'F 3 "" H 100 100 10  0000 C CNN',
                    '\t1 0 0',
                    '\t0 0 0 0',
                    '$EndComp',
                    '$EndSCHEMATC'
                ].join('\n');
                
                expect((<ComponentItem>parse(sch).items[0]).fields[0]).not.to.have.property('name');

           });

           it('Not all mandatory fields specificed', function() {
                const sch = [
                    ...fileHeader,
                    '$Comp',
                    'L 74xx_IEEE:7400 U?',
                    'U 4 1 5CC6394D',
                    'P 1111 2222',
                    'F 0 "" H 100 100 10  0000 C CNN',
                    'F 1 "" V 100 100 10  0000 C CNN',
                    'F 3 "" H 100 100 10  0000 C CNN',
                    '\t1 0 0',
                    '\t0 0 0 0',
                    '$EndComp',
                    '$EndSCHEMATC'
                ].join('\n');

                expect(sch).to.failParsingWith("Not all mandatory fields are defined")

           });

           it('Non-mandatory field default name', function() {
                const sch = [
                    ...fileHeader,
                    '$Comp',
                    'L 74xx_IEEE:7400 U?',
                    'U 4 1 5CC6394D',
                    'P 1111 2222',
                    'F 0 "" H 100 100 10  0000 C CNN "FieldName"',
                    'F 1 "" V 100 100 10  0000 C CNN',
                    'F 2 "" H 100 100 10  0001 C CNN',
                    'F 3 "" H 100 100 10  0000 C CNN',
                    'F 4 "" H 100 100 10  0000 C CNN',
                    '\t1 0 0',
                    '\t0 0 0 0',
                    '$EndComp',
                    '$EndSCHEMATC'
                ].join('\n');

                expect(sch).to.parseAs({
                    items: [
                        {
                            fields: [
                                { index: 0 },
                                { index: 1 },
                                { index: 2 },
                                { index: 3 },
                                {
                                    index: 4,
                                    name: 'Field4'
                                }
                            ]
                        }
                    ]
                });

           });

           it("Too many fields", function() {
                const extraFields = []
                for(let i = 4; i < 101; i++)
                    extraFields.push(`F ${i} "i" H 100 100 10 0000 C CNN`);

                expect(sch([], extraFields)).to.failParsingWith("Too many fields");
           });


        });

        describe('Transformation Matrix', function() {
            const sch = (x1, y1, x2, y2) => ([
                ...fileHeader,
                '$Comp',
                'L 74xx_IEEE:7400 U?',
                'U 4 1 5CC6394D',
                'P 1111 2222',
                'F 0 "" H 100 100 10  0000',
                'F 1 "" V 100 100 10  0000 R T',
                'F 2 "" H 100 100 10  0001 C CNN',
                'F 3 "" H 100 100 10  0000 C CNN',
                `\t1 0 0`,
                `\t${x1} ${y1} ${x2} ${y2}`,
                '$EndComp',
                '$EndSCHEMATC'
            ].join('\n'));

            it('Correct', function() {
                expect(sch(-1, 0, 1, -1)).to.parseAs({
                    items: [
                        {
                            type: ItemType.Component,
                            transform: {
                                x1: -1,
                                y1: 0,
                                x2: 1,
                                y2: -1
                            }
                        }
                    ]
                });
            })

            it('Out-of-range', function() {
                expect(sch(-2, 0, 0, 0)).to.failParsingWith('Invalid transform value');
                expect(sch(0, 2, 0, 0)).to.failParsingWith('Invalid transform value');
                expect(sch(0, 0, -3, 0)).to.failParsingWith('Invalid transform value');
                expect(sch(0, 0, 0, 3)).to.failParsingWith('Invalid transform value');
            })

        })

    });

    describe('Text Item', function() {
        const sch = (lines) => ([
            ...fileHeader,
            ...lines,
            '$EndSCHEMATC'
        ]).join('\n');

        it('Label', function() {
            expect(sch([
                'Text Label 1300 6950 0    40   ~ 0',
                'GPIO0'
            ])).to.parseAs({
                items: [
                    {
                        type: ItemType.Text,
                        textType: TextItemType.Label,
                        text: 'GPIO0',
                        position: { x: 1300, y: 6950 },
                        orientation: TextOrientation.Horizontal,
                        size: 40,
                        italic: false,
                        thickness: 0
                    }
                ]
            });
        });

        it('Hierarchical Label', function() {
            expect(sch([
                'Text HLabel 100 200 1   40 Input  Italic 10',
                'TextText ~~~"""'
            ])).to.parseAs({
                items: [
                    {
                        type: ItemType.Text,
                        textType: TextItemType.HierLabel,
                        text: 'TextText ~~~"""',
                        position: { x: 100, y: 200 },
                        orientation: TextOrientation.VerticalUp,
                        size: 40,
                        italic: true,
                        thickness: 10,
                        shape: SheetLabelType.Input
                    }
                ]
            });
        });

        it('Global Label', function() {
            expect(sch([
                'Text GLabel 500 600 2    40 ???   ~ 0',
                'XXX[0..10]'
            ])).to.parseAs({
                items: [
                    {
                        type: ItemType.Text,
                        textType: TextItemType.GlobalLabel,
                        text: 'XXX[0..10]',
                        position: { x: 500, y: 600 },
                        orientation: TextOrientation.HorizontalInvert,
                        size: 40,
                        italic: false,
                        thickness: 0,
                        shape: SheetLabelType.Unknown
                    }
                ]
            });
        });

        it('Notes', function() {
            expect(sch([
                'Text Notes -100 -200 3    40   ~ 0',
                'AB\\nCD'
            ])).to.parseAs({
                items: [
                    {
                        type: ItemType.Text,
                        textType: TextItemType.Notes,
                        text: 'AB\nCD',
                        position: { x: -100, y: -200 },
                        orientation: TextOrientation.VerticalDown,
                        size: 40,
                        italic: false,
                        thickness: 0
                    }
                ]
            });
        });

        it('Shape not specified', function() {
            expect(sch([
                'Text GLabel 500 600 2    40   ~ 0',
                'ABCDEFG'
            ])).to.failParsingWith('Shape must be specified for this type of text');
        });

        it('Shape specified when it shouldn\'t', function() {
            expect(sch([
                'Text Notes 500 600 2    40 BiDi  ~ 0',
                'ABCDEFG'
            ])).to.failParsingWith('Shape must not be specified for this type of text');
        });

        it('No text style, no thickness', function() {
            expect(sch([
                'Text Notes 500 600 2    40',
                'ABCDEFG'
            ])).to.parseAs({
                items: [
                    {
                        type: ItemType.Text,
                        italic: false,
                        thickness: 0,
                    }
                ]
            });
        });

        it('No thickness', function() {
            expect(sch([
                'Text Notes 500 600 2    40 Italic',
                'ABCDEFG'
            ])).to.parseAs({
                items: [
                    {
                        type: ItemType.Text,
                        italic: true,
                        thickness: 0,
                    }
                ]
            });
        });

    });

    it('Junction Item', function() {
        expect([
            ...fileHeader,
            'Connection ~ -100 100',
            '$EndSCHEMATC'
        ].join('\n')).to.parseAs({
            items: [
                {
                    type: ItemType.Junction,
                    position: { x: -100, y: 100 }
                }
            ]
        })
    });

    it('No Connection Item', function() {
        expect([
            ...fileHeader,
            'NoConn ~ -100 100',
            '$EndSCHEMATC'
        ].join('\n')).to.parseAs({
            items: [
                {
                    type: ItemType.NoConnect,
                    position: { x: -100, y: 100 }
                }
            ]
        })
    });

    describe('Line Item', function() {

        const sch = (extra) => ([
            ...fileHeader,
            ...extra,
            '$EndSCHEMATC'
        ]).join('\n');

        it('Line types', function() {

            expect(sch([
                "Wire Wire Line",
                "  0 0 0 0"
            ])).to.parseAs({
                items: [{ type: ItemType.Line, lineType:LineType.Wire }]
            });

            expect(sch([
                "Wire Bus Line",
                "  0 0 0 0"
            ])).to.parseAs({
                items: [{ type: ItemType.Line, lineType:LineType.Bus }]
            });

            expect(sch([
                "Wire Notes Line",
                "  0 0 0 0"
            ])).to.parseAs({
                items: [{ type: ItemType.Line, lineType:LineType.Notes }]
            });

        });

        it('Line Coordinates', function() {
            expect(sch([
                "Wire Wire Line",
                "  10 20 -30 0"
            ])).to.parseAs({
                items: [{ 
                    type: ItemType.Line, 
                    start: { x: 10, y: 20 },
                    end: { x:-30, y: 0 }
                }]
            });
        });

        it('Line with width', function() {
            expect(sch([
                "Wire Wire Line  width 123",
                "  0 0 0 0"
            ])).to.parseAs({
                items: [{ 
                    type: ItemType.Line, 
                    width: 123
                }]
            });
        });

        it('Line with style', function() {
            const styles = [
                [ LineStyle.Solid, 'solid' ],
                [ LineStyle.Dashed, 'dashed' ],
                [ LineStyle.Dotted, 'dotted'],
                [ LineStyle.DashDot, 'dash_dot' ]
            ];

            for (const [styleConst, styleName] of styles) {
                expect(sch([
                    `Wire Wire Line style ${styleName}`,
                    '  0 0 0 0'
                ])).to.parseAs({
                    items: [{ 
                        type: ItemType.Line, 
                        style: styleConst
                    }]
                });    
            }

        });

        it('Line with color', function() {
            expect(sch([
                'Wire Wire Line rgb(10, 20, 30)',
                '  0 0 0 0'
            ])).to.parseAs({
                items: [{ 
                    type: ItemType.Line, 
                    color: { r: 10, g: 20, b: 30 }
                }]
            });

            expect(sch([
                'Wire Wire Line rgba(10, 20, 30, 40)',
                '  0 0 0 0'
            ])).to.parseAs({
                items: [{ 
                    type: ItemType.Line, 
                    color: { r: 10, g: 20, b: 30, alpha: 40 }
                }]
            });

        });

        it('Line with all props', function() {
            expect(sch([
                'Wire Wire Line width 23 style dash_dot rgba(10, 20, 30, 40)',
                '  0 0 0 0'
            ])).to.parseAs({
                items: [{ 
                    type: ItemType.Line, 
                    width: 23,
                    style: LineStyle.DashDot,
                    color: { r: 10, g: 20, b: 30, alpha: 40 }
                }]
            });

        });

        it('Line with invalid color', function() {
            expect(sch([
                'Wire Wire Line rgb(310, 20, 30)',
                '  0 0 0 0'
            ])).to.failParsingWith('Invalid color value')

        });

    });

    describe('Bitmap Item', function() {

        const toHex = (v: number) => ((v < 16)? "0" + v.toString(16) : v.toString(16)).toUpperCase();
        const sch = ({position, scale, data}: {position?: Position, scale?: string, data?: Buffer}) => {

            // Split the data into chunks
            const chunks = [];

            if (data) {
                let currentChunk = null;
                for(let i = 0; i < data.length; i++) {
                    if ((i % 16) === 0) {
                        if (currentChunk)
                            chunks.push(currentChunk)
                        currentChunk = []
                    }
                    currentChunk.push(data[i]);
                }
                chunks.push(currentChunk);   
            }
            else
                chunks.push([0]);

            // Encode them
            const dataLines = chunks.map((dl:number[]) => dl.map((v: number) => toHex(v)).join(' '));

            return ([
                ...fileHeader,
                '$Bitmap',
                `Pos ${position? position.x : 0} ${position? position.y : 0}`,
                `Scale ${scale || 0}`,
                'Data',
                ...dataLines,
                'EndData',
                '$EndBitmap',
                '$EndSCHEMATC'
            ]).join('\n');
        }
        
        

        it('Position', function() {
            expect(sch({ position: { x: -1000, y: 1000}})).to.parseAs({
                items: [{ 
                    type: ItemType.Bitmap, 
                    position: { x: -1000, y: 1000 },
                }]
            });
        });

        it('Scale', function() {
            const bitmapWithScale = (scale: number) => ({
                items: [{ 
                    type: ItemType.Bitmap, 
                    scale,
                }]
            });

            expect(sch({ scale: '0' })).to.parseAs(bitmapWithScale(0));
            expect(sch({ scale: '1.2' })).to.parseAs(bitmapWithScale(1.2));
            expect(sch({ scale: '0.5' })).to.parseAs(bitmapWithScale(0.5));
            expect(sch({ scale: '1E3' })).to.parseAs(bitmapWithScale(1E3));
            expect(sch({ scale: '2.4E-3' })).to.parseAs(bitmapWithScale(2.4E-3));
        });

        it('Data (synthetic)', function() {

            const data = Buffer.alloc(1000);
            for(let i = 0; i < data.length; i++) {
                data[i] = Math.random()*255;
            }

            expect(sch({ data })).to.parseAs({
                items: [
                    {
                        type: ItemType.Bitmap,
                        data: data.toString('base64')
                    }
                ]
            });
        });

        xit('Data (real)', async function() {
            // Use a real schematic that embeds a png file
            const sch = await readFile(resolve(__dirname, 'resources', 'logo.sch'));
            const png = await readFile(resolve(__dirname, 'resources', 'logo.png'));

            expect(sch.toString()).to.parseAs({
                items: [
                    {
                        type: ItemType.Bitmap,
                        position: { x: 5850, y: 3550 },
                        scale: 1.0,
                        data: png.toString('base64')
                    }
                ]
            })
        });

    });

    describe('Entry Item', function() {

        const sch = (extra ) => ([
            ...fileHeader,
            ...extra,
            '$EndSCHEMATC'
        ]).join('\n');

        it("Wire Entry", function() {
            expect(sch([
                "Entry Wire Line",
                "  1000 2000 1100 2200"
            ])).to.parseAs({
                items: [
                    {
                        type: ItemType.BusEntry,
                        busEntry: false,
                        position: { x: 1000, y: 2000 },
                        size: { x: 100, y: 200 }
                    }
                ]
            });
        });

        it("Bus Entry", function() {
            expect(sch([
                "Entry Bus Bus",
                "  1000 2000 1100 2200"
            ])).to.parseAs({
                items: [
                    {
                        type: ItemType.BusEntry,
                        busEntry: true,
                        position: { x: 1000, y: 2000 },
                        size: { x: 100, y: 200 }
                    }
                ]
            });
        });

    });

    describe('Sheet Item', function() {

        const sch = (sheet) => ([
            ...fileHeader,
            '$Sheet',
            ...sheet,
            '$EndSheet',
            '$EndSCHEMATC'
        ]).join('\n');

        it('Position, Size, Name, Filename, Timestamp', function() {

            expect(sch([
                'S 1000 2000 3000 4000',
                'U 1234ABCD',
                'F0 "NameA" 100',
                'F1 "FilenameB" 200'
            ])).to.parseAs({
                items: [
                    {
                        type: ItemType.Sheet,
                        position: { x: 1000, y: 2000 },
                        size: { x: 3000, y: 4000 },
                        timestamp: '1234ABCD',
                        name: 'NameA',
                        nameSize: 100,
                        filename: 'FilenameB',
                        filenameSize: 200,
                        pins: []
                    }
                ]
            })
        });

        it('Pins', function() {
            expect(sch([
                'S 1000 2000 3000 4000',
                'U 1234ABCD',
                'F0 "" 100',
                'F1 "" 200',
                'F2 "GPIO0" B L 2950 950 60',
                'F3 "GPIO1" I T 1000 -2000 10',
                'F4 "GPIO2" O R -1000 2000 10',
                'F5 "GPIO3" T B 0 0 0',
                'F6 "GPIO4" U B 0 0 0'
            ])).to.parseAs({
                items: [
                    {
                        type: ItemType.Sheet,
                        pins: [
                            {
                                number: 2,
                                name: 'GPIO0',
                                side: SheetPinSide.Left,
                                shape: SheetLabelType.BiDi,
                                position: { x: 2950, y: 950 },
                                size: 60
                            },
                            {
                                number: 3,
                                name: 'GPIO1',
                                side: SheetPinSide.Top,
                                shape: SheetLabelType.Input,
                                position: { x: 1000, y: -2000 },
                                size: 10
                            },
                            {
                                number: 4,
                                name: 'GPIO2',
                                side: SheetPinSide.Right,
                                shape: SheetLabelType.Output,
                                position: { x: -1000, y: 2000 },
                                size: 10
                            },
                            {
                                number: 5,
                                name: 'GPIO3',
                                side: SheetPinSide.Bottom,
                                shape: SheetLabelType.ThreeState,
                                position: { x: 0, y: 0 },
                                size: 0
                            },
                            {
                                number: 6,
                                name: 'GPIO4',
                                side: SheetPinSide.Bottom,
                                shape: SheetLabelType.Unspecified,
                                position: { x: 0, y: 0 },
                                size: 0
                            }

                        ]
                    }
                ]
            })

        });

        it('Invalid pin number', function() {

            expect(sch([
                'S 1000 2000 3000 4000',
                'U 1234ABCD',
                'F0 "" 100',
                'F1 "" 200',
                'F1 "name" B L 1000 1000 10'
            ])).to.failParsingWith('Invalid sheet pin number')
        });


    });
})
