/// <reference path="./unit/assertions.d.ts"/>

import { readFile } from 'fs-extra';
import { basename } from 'path';
import * as chai from 'chai';
import kischHelper from './unit/assertions';
import { schematics } from '@kihub/test-files';

chai.use(kischHelper);

const { expect } = chai;

declare var CMPL_LIB_DIR: string;

describe("Compatibility with existing libs", function() {
 
    schematics.forEach(schFile => {

        const displayName = basename(schFile);
        it(`Schematic '${displayName}' parses`, async function() {

            const rawData = await readFile(schFile);
            expect(rawData.toString()).to.parseWithoutError();
        });

    });

});

